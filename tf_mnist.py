import tensorflow as tf
import numpy as np
import cv2, random

# Load MNIST dataset from keras library.
from keras.utils import to_categorical
from keras.datasets import mnist

epoch = 100
batchSize = 2048
learning_rate = 0.01

testingExampleCount = 10
imageScale = 10

# Load data as Numpy arrays.
(xtrain, ytrain), (xtest, ytest) = mnist.load_data()

# Reshape images. Shape: (Examples, Width, Height, Channel)
# Width and Height is 28 in MNIST dataset.
# And channel count is 1, which means images are grayscale.
xtrain = np.reshape(xtrain, (xtrain.shape+(1,))).astype(float)
xtest_d = np.reshape(xtest, (xtest.shape+(1,))).astype(float)

# Make targets one-hot vectors.
ytrain = to_categorical(ytrain).astype(float)
ytest_d = to_categorical(ytest).astype(float)

# Print shape of the dataset.
print("X train:", xtrain.shape)
print("y train:", ytrain.shape)
print("X test:", xtest_d.shape)
print("y test:", ytest_d.shape)

# Define model.
sess = tf.Session()

# xx is input to model, and yy is the target value of the model. (Just for one batch, or example.)
xx = tf.placeholder(tf.float32, shape=(None, 28, 28, 1))	# 'None' means there would be any number of examples.
yy = tf.placeholder(tf.float32, shape=(None, 10))

# Define model.
# Conv > MaxPool > Conv > Flatten > FC-512 > FC-64 > FC-10 > Softmax
# ReLU activation function in Convolution and Fully-Connected layers.

conv1 = tf.layers.conv2d(xx, filters=8, kernel_size=[5, 5], padding="valid", activation=tf.nn.relu)
mp1 = tf.layers.max_pooling2d(conv1, pool_size=[2, 2], strides=2)
conv2 = tf.layers.conv2d(mp1, filters=16, kernel_size=[5, 5], padding="valid", activation=tf.nn.relu)

flat = tf.layers.flatten(conv2)
fc1 = tf.layers.dense(flat, units=512, activation=tf.nn.relu)
fc2 = tf.layers.dense(fc1, units=64, activation=tf.nn.relu)
logit = tf.layers.dense(fc2, units=10)

loss = tf.losses.softmax_cross_entropy(onehot_labels=yy, logits=logit)

"""
losses_softmax_cross_entropy
Takes the output of the model (pred variable in this case) WITHOUT the Softmax activation.
And applies it to Softmax layer than calculates loss with Softmax's output.
"""

# This tensor is for prediction process.
# Takes the logits (Logit: Outputs WITHOUT Softmax.), and applies it Softmax.
pred = tf.nn.softmax(logit)

optimizer = tf.train.GradientDescentOptimizer(learning_rate)
train = optimizer.minimize(loss)	# Define training tensor.

sess.run(tf.global_variables_initializer())

dataIndex = 0
for i in range(0, epoch):
	_xx = xtrain[dataIndex:(dataIndex+batchSize)]	# Mini-Batch defined to temporary variables.
	_yy = ytrain[dataIndex:(dataIndex+batchSize)]
	
	# Add batchSize for next batch.
	dataIndex += batchSize
	
	# If index exceeds count of examples in dataset, it should get back to zero.
	if (dataIndex+batchSize) > xtrain.shape[0]:
		dataIndex = 0
	
	# Train model with our Mini-Batch.
	sess.run(train, feed_dict={xx:_xx, yy:_yy})
	
	# Print epoch at every %1 progress.
	if i%(epoch/100) == 0:
		print("Epoch:", i)

# Calculate final loss with test set.
print("Testing Loss:", sess.run(loss, feed_dict={xx:xtest_d, yy:ytest_d}))

# Test the model with test dataset.
for i in range(0, testingExampleCount):
	# Pick a random example from test set.
	ri = random.randint(0, xtest_d.shape[0])
	
	# Taking slice in this way because of input shape. (1, 28, 28, 1)
	_xx = xtest_d[ri:ri+1]
	_yy = ytest_d[ri:ri+1]
	
	# During training, Softmax applied to outputs by loss function.
	# In testing process, we should manually apply Softmax.
	# Pred tensor does this.

	# Feed example to the model and get the output.
	_output = sess.run(pred, feed_dict={xx:_xx, yy:_yy})[0]

	predicted = np.argmax(_output)
	target = np.argmax(_yy)
	
	print("Predicted", predicted, "Target", target)
	
	# Ignore the batch size (already 1) while showing it.
	img = xtest[ri:ri+1][0]
	
	# Resize the image for showing it in different scale.
	img = cv2.resize(img, (28*imageScale, 28*imageScale))
	
	title = "Predicted " + str(predicted) + " | Target " + str(target)
	
	# Show the resized image.
	cv2.imshow(title, img)
	cv2.waitKey(0)
	cv2.destroyAllWindows()
